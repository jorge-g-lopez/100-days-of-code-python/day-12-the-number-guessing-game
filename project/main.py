"""The number guessing game"""

import random

LOWER = 1
UPPER = 100
EASYLEVEL = 10
HARDLEVEL = 5
QUESTION = "Choose a difficulty. Type 'easy' or 'hard': "
endofgame = False
thinkingof = random.randint(LOWER, UPPER)

print("Welcome to the Number Guessing Game!")
print(f"I'm thinking of a number between {LOWER} and {UPPER}.")
difficulty = input(QUESTION).lower()

if difficulty == "easy":
    attempts = EASYLEVEL
else:
    attempts = HARDLEVEL

while endofgame is False:
    if attempts > 0:
        print(f"You have {attempts} attempts remaining to guess the number.")
        guess = int(input("Make a guess: "))
        attempts -= 1

        if guess == thinkingof:
            print(f"You got it! The answer was {thinkingof}.")
            endofgame = True
        elif guess > thinkingof:
            print("Too high.")
        else:
            print("Too low.")

        if attempts > 0 and endofgame is False:
            print("Guess again.")
    else:
        endofgame = True
        print("You've run out of guesses, you lose.")
